	<?php
		/*Lisez d'abord à partir de la ligne 20*/
        function all($directory) { /*Cette fonction va permettre d'avoir toutes les musiques d'un répertoire y compris celles contenu dans les dossiers du répertoire*/
		global $tab;				/*On récupére le tableau initialisé tout à l'heure, pour ne pas perdre de données entre chaque appel de fonction*/
                $musique=scandir($directory);	/*On liste les fichiers du répertoire*/
                for ($i=2; $i<count($musique); $i++) {	/*On regarde tous les fichiers*/
                        $extend3 = explode(".", $musique[$i]); /*On prend l'extension*/
                        $extend4=$extend3[count($extend3)-1];	/*Dans $extend4*/
                        if (($extend4 == "mp3")||($extend4 == "m4a")) {/*On regarde si c'est une musique*/
							array_push($tab ,array(	/*si c'est une musique on l'insert dans le tableau comme tout à l'heure*/
								"type" => 0,
								"path" => $directory."/".$musique[$i],
								"name" => $musique[$i]
							));
                        } else if(is_dir($directory."/".$musique[$i])) { 	/*Si c'est un dossier, on rappel la fonction all sur ce dossier*/
                               all($directory."/".$musique[$i]);			/*Pour avoir les musiques de ce dossier, on appel cela un appel*/
                        }													/*récursif de la fonction all()*/	
					}
   		}
	if ($_GET['folder']!='all') { /*si l'utilisateur ne demande pas toutes les musiques on regarde quel dossier il veut*/
		$repertory ="../music/".$_GET['folder'];
            $fichier=scandir($repertory); /*On regarde ce qu'il y a dans le dossier, la fonction scandir renvoit un tableau que l'on met dans la variable $fichier*/
            for($i = 2; $i < count($fichier); $i++) { /*on parcourt le tableau grâce à une boucle for qui commence à 2, les 2 premiers éléments sont */
            											/*.(adresse tu dossier dans lequel on est) et ..(adresse du dossier parent) */
                $extend=explode(".", $fichier[$i]);/*On manipule le nom du fichier pour n'avoir*/
                $extend2 = $extend[count($extend)-1];/*que l'extension; $extend2 contient l'extension*/
                if (($extend2 == "mp3")||($extend2 == "m4a")) { /*On regarde l'extension pour savoir si le fichier est bien une musique*/
					$tab[$i] = array(									/*On met dans le tableau la musique*/
						"type" => 0,									/*type=0 parce que c'est une musique*/
						"path" => $repertory.$fichier[$i],				/*le chemin pour accéder à la musique*/
						"name" => $fichier[$i]							/*Le nom de la musique*/
					);
                } else if(is_dir($repertory."/".$fichier[$i])) {		/*si c'est pas une musique, on regarde si c'est un dossier*/
                    $tab[$i] = array(									/*On met le dossier dans le tableau*/
						"type" => 1,									/*type=1 car c'est un dossier*/
						"path" => $repertory.$fichier[$i],				/*Le chemin du dossier dans path*/
						"name" => $fichier[$i]							/*Le nom du dossier*/
					);
				}
            }
		header("Content-Type: text/json"); /*On spécifie que le fichier renvoyé est un JSON */
		echo json_encode($tab);				/*On renvoit le JSON à la page qui l'a demandé grâce à echo et json_encode*/
	} else { 								/*sinon l'utilisteur demande toutes les musique*/
		$repertory="../music";				/*on définit le répertoire où toutes les musiques sont contenues*/
		$tab = array();						/*On initialise le tableau*/
		all($repertory);					/*On appelle la fonction all définie au début, vous pouvez aller la voir maintenant, on lui passe en paramètre le dossier à fouiller*/
		header("Content-Type: text/json");	/*On spécifie que le fichier renvoyé est un JSON*/
		echo json_encode($tab);				/*On envoit le json*/
	}
