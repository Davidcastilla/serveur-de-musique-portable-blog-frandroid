<html>									<!--On commence toujours une page en ouvrant la balise HTML-->
	<head>								<!--Tout ce qui est dans le head ne s'affiche pas, on s'en sert pour définir certaines caractéristiques de la page-->
		<title>Musique</title>			<!--On éfinit le titre de la page, qui va s'afficher comme nom d'onglet-->
		<meta charset="utf-8"/>			<!--On définit l'encodage des caractères de la page-->
		<link href="css/music.css" type="text/css" rel="stylesheet"/>	<!--On importe le CSS-->
		<script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>	<!--On importe jquery pour utiliser le javascript plus tard-->
	</head>
	<body>					<!--Le body est ce qui va s'afficher dans la page-->
		<button onClick="request('')">Page d'accueil</button><br/>	<!--Ceci est un bouton, le onClick est la fonction que l'on va appeler en JS-->
		<div id="choice"/>			<!--Dans cette div, nous afficherons les musiques-->
		</div>
		<div id="player">			<!--Dans celle-là, nous allons afficher tous les contrôles de la musique-->
			<audio ontimeupdate="update(this)" id="audioPlayer" src="./music/une_musique.mp3" ></audio>	<!--audio est l'objet qui va lire la musique-->
			<button class="control" id="retour" onClick="retour()"><img src="retour.png"/></button><!--Bouton de musique précédente-->
			<button class="control" id="play" onClick="play('audioPlayer', this)"><img id="icon" src="play.png"/></button><!--Bouton Play-->
			<button class="control" id="stop" onClick="resume('audioPlayer')"><img src="stop.png"/></button><!--Bouton STOP-->
			<button class="control" id="suivant" onClick="suivant()"><img src="suivant.png"/></button><!--Bouton pause-->
			<button class="control" id="random" onClick="rando()"><img src="random.png"</button><!--Bouton pour mettre la lecture en aléatoire-->
			<button class="control" onClick="request('all')">Toutes les musiques</button><br/><!--Bouton pour afficher toutes les musiques de music/-->
			<div id="progressBarControl"  onclick="clickProgress('audioPlayer', this, event)"><!--Cette div va contenir la barre de progression de la musique-->
				<div id="progressBar">Pas de lecture</div><!--La barre de progression, on  définit son texte à "Pas de lecture", on le changera grâce au JS-->
			</div><br/><!--la balise br permet un retour à la ligne-->
			<div id="title"></div><!--Dans cette div, nous allons afficher le titre de la musique en lecture-->
		</div>
	</body>
</html>
<!--Les fonctions appelées dans les onClick sont succeptibles de changer, cette page est une ancienne version de mon projet-->
<!--Depuis, le javascript a changé-->
<!--Les balises img dans les buttons sont là pour définir les images des boutons-->